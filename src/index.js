const hiHat = document.getElementById('hi-hat');
const snare = document.getElementById('snare');
const bass = document.getElementById('bass');

// snare.onclick = () => {
//   console.log('snare!!!');
// };

document.addEventListener('keydown', handleDrumHit);

function handleDrumHit({ key }) {
  switch (key.toLowerCase()) {
    case 'i': {
      const ride = document.createElement('audio');
      ride.src = './jazz-ride.wav';
      ride.autoplay = true;
      document.body.appendChild(ride);
      setTimeout(() => {
        document.body.removeChild(ride);
      }, 1500);

      console.log('here');
    }
  }
}
